# Natura

Pornografia internetowa działa poprzez przejęcie kontroli nad naturalnymi mechanizmami nagrody, zaprojektowanych tak, abyś mógł się rozmnażać jak najdłużej. Natychmiastowa i łatwo dostępna forma pornografii internetowej sprawia, że mózgowy mechanizm nagrody produkuje dopaminę znacznie dłużej niż normalnie jest to możliwe. Naukowo nazywa się to efektem Coolidge'a, o którym być może już wiesz.

Dopamina jest neuroprzekaźnikiem związanym z uczuciami pragnienia, z rzeczywistą przyjemnością wytwarzaną przez opioidy. Więcej dopaminy, więcej opioidów i więcej działania. Bez dopaminy, czynności takie jak jedzenie nie sprawiałyby nam przyjemności, a żywność o wysokiej zawartości tłuszczu i cukru zapewnia najwyższe uwalnianie tej substancji.

Dopamina jest również uwalniana w odpowiedzi na nowość. Przy pozornie nieskończonej ilości dostępnej pornografii zalewa ona układ limbiczny (obwód nagrody), więc kiedy pierwszy raz zobaczysz porno, działasz, dochodzisz do orgazmu i wyzwalasz kolejną powódź opioidów. Zmotywowany, aby uzyskać jak najwięcej dopaminy jak to możliwe, mózg przechowuje to jako skrypt dla łatwego przypomnienia i wzmacnia ścieżki neuronalne poprzez uwolnienie substancji chemicznej zwanej DeltaFosB. Mózg wywołuje ścieżki w odpowiedzi na sygnały takie jak seksowne reklamy,chwila w samotności, stres lub nawet uczucie lekkiego doła i nagle jesteś gotowy do przejażdżki na 'wodnej zjeżdżalni'. Za każdym razem gdy ten schemat się powtarza, więcej DeltaFosB jest uwalniane, więc zjeżdżalnia jest nasmarowana, żywa i łatwiejsza do zjechania następnym razem.

Układ limbiczny posiada system samokorygujący, który przycina liczbę receptorów dopaminowych i opioidowych w przypadku wykrycia częstego i codziennego zalewu dopaminy. Niestety, receptory te są również potrzebne, aby utrzymać naszą motywację do radzenia sobie z codziennym stresem w życiu. Standardowe ilości dopaminy wytwarzane przez naturalne nagrody po prostu nie dorównują pornografii i nie są tak skutecznie absorbowane przez zmniejszone receptory, co prowadzi do tego, że czujesz się bardziej zestresowany i rozdrażniony niż normalnie. Proces ten znany jest jako desensytyzacja.

W tym cyklu przekroczyłeś "czerwoną linię" i wywołałeś emocje takie jak poczucie winy, obrzydzenie, zakłopotanie, niepokój i strach, które z kolei podnoszą poziom dopaminy jeszcze wyżej i powodują, że mózg błędnie interpretuje te uczucia jako podniecenie seksualne.

Wraz z upływem czasu, mózg nie tylko znieczula się na poprzednio widziane klipy, ale także na podobne gatunki i poziom szoku. Ten spadek motywacji wyzwala uczucie mniejszej satysfakcji, ponieważ nasz mózg angażuje się w ciągłe ocenianie, popychając Cię do szukania klipów które zaspokoją głód. Szukasz więc więcej nowości, klikając na amatorski, wywołujący szok klip na stronie głównej o którym pewnie powiedziałeś, że nie zrobisz tego przy pierwszej wizycie. 



![The Coolidge Effect graphic](images/coolidge.png)
(note - getting the background changed, sozzle. view the image manually or change the background to the light version in the 'A' menu up the top)

> *"W rosie małych rzeczy serce odnajduje poranek i jest świeżość"* - correct later
>
> --- Kahlil Gibran

Przelotne poczucie bezpieczeństwa jest wszystkim, co jest potrzebne aby przejść przez trudny punkt w życiu, ale czy twój odczulony mózg będzie w stanie złapać tę kroplę odstresowywacza, którą mózg nieużywający jest w stanie wykorzystać?

Zalew dopaminy działa jak błyskawicznie działający narkotyk, szybko opadając i wywołując bóle odstawienia. Wielu użytkowników ma złudzenie, że te bóle to straszna trauma, którą przeżywają próbując lub będąc zmuszonym do odstawienia. W rzeczywistości są one przede wszystkim psychiczne, ponieważ użytkownik czuje się pozbawiony ich przyjemności.


## Mały potwór

Rzeczywiste chemiczne odstawienie porno jest tak subtelne, że większość użytkowników żyła i umarła nie zdając sobie sprawy, że są narkomanami. Wielu użytkowników boi się narkotyków, ale to jest dokładnie to, czym są - są narkomanami. Na szczęście jest to narkotyk łatwy do odstawienia, ale najpierw musisz zaakceptować fakt, że w rzeczywistości jesteś uzależniony. Odstawienie porno nie powoduje żadnego fizycznego bólu i jest jedynie pustym, niespokojnym uczuciem braku czegoś, dlatego wielu wierzy, że ma to coś wspólnego z pożądaniem seksualnym. Jeśli się przedłuża, przeradza się w nerwowość, niepewność, pobudzenie, niska pewność siebie i drażliwość. To jest jak głód dla trucizny.

W ciągu kilku sekund od rozpoczęcia sesji, dopamina jest dostarczana, głód się kończy co skutkuje uczuciem spełnienia, gdy zjeżdżasz z wodnej zjeżdżalni. W pierwszych dniach, bóle odstawienne i ich następująca po nich ulga są tak nieznaczne, że nie zdajemy sobie z nich sprawy. Kiedy stajemy się regularnymi użytkownikami wierzymy, że to dlatego, że polubiliśmy je lub weszliśmy w "nawyk". Prawda jest taka, że jesteśmy już uzależnieni, ale nie zdajemy sobie z tego sprawy. Mały potwór jest już w naszych mózgach, więc od czasu do czasu robimy sobie wycieczki w dół wodnej zjeżdżalni, aby go nakarmić.

Wszyscy użytkownicy zaczynają szukać porno z irracjonalnych powodów. *Jedynym* powodem, dla którego ktokolwiek kontynuuje korzystanie z porno, niezależnie od tego czy jest zwykłym czy intensywnym użytkownikiem, jest karmienie tego małego potwora. Całe to zamieszanie jest serią okrutnych i mylących kar, ale być może najbardziej żałosnym aspektem jest poczucie przyjemności, jakie użytkownik czerpie z sesji, próbując wrócić do poczucia spokoju, wyciszenia i pewności siebie, jakie jego ciało miało przed uzależnieniem się w pierwszej kolejności. 


## Irytujący dźwięk alarmu

Znasz to uczucie, gdy sąsiadowi przez cały dzień wył alarm w samochodzie -- lub jakaś inny drobna uporczywa udręka -- gdy hałas nagle zatrzymuje i czujesz cudowne uczucie ciszy i spokoju? To nie jest naprawdę spokój, ale zakończenie uciążliwości. Przed rozpoczęciem kolejnej sesji nasze ciała są kompletne, ale potem zaczynamy zmuszać nasze mózgi do pompowania dopaminy, a kiedy skończymy i zaczyna ona odchodzić, odczuwamy bóle odstawienia. Nie jest to ból fizyczny, a jedynie uczucie pustki. Nie jesteśmy nawet świadomi jego istnienia, ale jest to jak kapiący kran wewnątrz naszego ciała.

Nasz racjonalny umysł tego nie rozumie, ale wcale nie musi rozumieć. Wszystko co wiemy to to, że chcemy porno i kiedy się masturbujemy, pragnienie mija. Satysfakcja jest jednak ulotna, bo aby zaspokoić głód, potrzeba więcej porno. Jak tylko osiągniesz orgazm, pragnienie zaczyna się na nowo i pułapka nadal cię trzyma. To pętla sprzężenia zwrotnego, chyba że zdecydujesz się ją przerwać!

Pułapka porno jest podobna do noszenia ciasnych butów tylko po to, aby uzyskać przyjemność z ich zdejmowania. Istnieją trzy podstawowe powody, dla których użytkownicy nie potrafią spojrzeć na to w ten sposób.

1.	Od urodzenia jesteśmy poddawani masowemu praniu mózgu które mówi nam, że porno w internecie to po prostu kolejny postęp który zastąpił drukowaną wersję porno. Ten fałsz jest opakowany w prawdę, że masturbacja nie jest szkodliwa, więc dlaczego nie mielibyśmy im wierzyć?

2.	Ponieważ fizyczne odstawienie dopaminy nie wiąże się z żadnym bólem, a jedynie pustym, niepewnym uczuciem które niczym nieróżni się od głodu i normalnego stresu, to uczucie objawia się w sesji porno, ponieważ w takich właśnie momentach szukamy pornografii internetowej. Zwykle traktujemy to uczucie jako normalne.

3.	Jednak głównym powodem, dla którego użytkownicy nie dostrzegają pornografii internetowej w jej prawdziwym świetle jest to, że działa ona od tyłu do przodu. To właśnie wtedy, gdy go nie konsumujesz, cierpisz na uczucie pustki. Ponieważ proces uzależniania się jest niezwykle subtelny i stopniowy w początkowym okresie, uczucie pustki jest uważane za normalne i nie jest przypisywane poprzedniej sesji. W momencie odpalenia przeglądarki i rozpoczęcia sesji dostajesz natychmiastowy zastrzyk energii i stajesz się mniej nerwowy lub bardziej zrelaksowany, więc porno internetowe dostaje zasługę.

Ten odwrotny proces "końca do początku" sprawia, że wszystkie narkotyki są trudne do odstawienia. Wyobraź sobie stan paniki osoby uzależnionej od heroiny, która nie ma heroiny; teraz wyobraź sobie radość, kiedy w końcu może wbić igłę w żyłę. Osoby nieuzależnione od heroiny nie cierpią z powodu tego panicznego uczucia.

Heroina nie łagodzi tego uczucia, ona je wywołuje. Podobnie nie-użytkownicy nie odczuwają pustych uczuć związanych z potrzebą oglądania porno w internecie, ani nie wpadają w panikę, kiedy jest awaria internetu. Nie-użytkownicy nie mogą zrozumieć, jak użytkownicy mogą czerpać przyjemność z dwuwymiarowych filmów z wyciszonymi dźwiękami i nienormalnymi proporcjami ciała. Ostatecznie, użytkownicy też nie mogą tego zrozumieć.

Mówimy o tym, że porno w internecie jest relaksujące lub satysfakcjonujące, ale jak można być zadowolonym, jeśli nie było się najpierw niezadowolonym? Osoba nie korzystająca z porno nie cierpi na ten niezadowolony stan, jest całkowicie zrelaksowana po randce bez seksu, podczas gdy użytkownik nie jest, dopóki nie zaspokoi swojego "małego potwora". 
nster’.

## Przyjemność czy twoja podpora?

Ważne przypomnienie - głównym powodem dla którego użytkownikom trudno jest rzucić, jest przekonanie, że rezygnują z prawdziwej przyjemności lub swojej podpory, swojego wsparcia. Istotne jest aby zrozumieć, że nie rezygnujesz z absolutnie niczego. Najlepszym sposobem na zrozumienie subtelności pułapki porno jest porównanie jej do jedzenia. Przyzwyczajenie do regularnych posiłków powoduje, że nie odczuwamy głodu pomiędzy nimi, uświadamiamy go sobie tylko wtedy, gdy posiłek się opóźnia. Nie ma fizycznego bólu, tylko puste, niepewne uczucie rozpoznawane jako głód. Proces zaspokajania naszego głodu jest bardzo przyjemnym doświadczeniem.

Pornografia wydaje się być niemal identyczna, ale tak nie jest. Podobnie jak w przypadku głodu, nie ma fizycznego bólu, a mechanizm nagrody zachowuje się podobnie, ale to właśnie podobieństwo do jedzenia sprawia, że użytkownik oszukuje się, że odczuwa prawdziwą przyjemność. Chociaż jedzenie i porno wydają się być bardzo podobne, w rzeczywistości są dokładnymi przeciwieństwami.

-   Jesz, żeby przetrwać i dodać sobie energii, podczas gdy porno przyćmiewa i obniża twoje mojo.

-   Jedzenie naprawdę dobrze smakuje i jest prawdziwie przyjemnym doświadczeniem, którym cieszymy się przez całe życie. Porno wiąże się z autosabotażem receptorów szczęścia i w ten sposób niszczy twoje szanse na szczęście oraz radzenie sobie z trudnościami.

-   Jedzenie nie wywołuje głodu i rzeczywiście go łagodzi, podczas gdy pierwsza i każda kolejna sesja porno uruchamia głód dopaminy. To nie przynosi ulgi, ale zapewnia cierpienie do końca życia.

Czy jedzenie to nawyk? Jeśli tak myślisz - spróbuj go całkowicie przełamać! Opisywanie jedzenia jako nawyku byłoby jak opisywanie oddychania jako nawyku - oba są niezbędne do przetrwania. To prawda, że ludzie mają zwyczaj zaspokajania głodu o różnych porach różnymi rodzajami jedzenia, ale samo jedzenie nie jest nawykiem. Podobnie jest z pornografią. Jedynym powodem dla którego użytkownik odpala przeglądarkę, jest próba zakończenia pustych uczuć jakie wywołała poprzednia sesja, w różnym czasie, z różną eskalacją gatunków porno.

W internecie porno jest często określane jako nawyk i dla wygody, EasyPeasy również odnosi się do "nawyku". Należy jednak stale mieć świadomość, że porno to nie nawyk, to uzależnienie od narkotyków! Kiedy zaczynamy używać porno, musimy się zmusić, aby sobie z nim poradzić. Zanim się zorientujemy, zaczynamy sięgać po coraz bardziej dziwaczne i szokujące porno. Dopamina szybko opuszcza organizm po orgazmie, co wyjaśnia, dlaczego użytkownicy chcą "krawędziować" (opóźniać orgazm), przerzucając się między wieloma oknami przeglądarki i zakładkami. 

## Przekroczenie czerwonej linii 

Tak jak w przypadku każdego innego narkotyku, organizm ma tendencję do uodparniania się na działanie tych samych starych klipów, nasz mózg pragnie więcej lub czegoś innego. Po krótkich okresach oglądania tego samego klipu przestaje on całkowicie łagodzić bóle odstawienne, które wywołała poprzednia sesja. W tym porno raju toczy się wojna, chcesz pozostać po bezpiecznej stronie swojej "czerwonej linii", ale twój mózg prosi cię o kliknięcie na zakazany owoc.

Czujesz się lepiej po tej sesji porno, ale jesteś bardziej nerwowy i mniej zrelaksowany niż ktoś, kto nigdy nie zaczął, mimo że żyjesz w rzekomym porno raju. Ta pozycja jest jeszcze bardziej niedorzeczna niż noszenie ciasnych butów, bo w miarę jak idziesz przez życie, po zdjęciu butów pozostaje coraz większy dyskomfort. Ponieważ użytkownik wie, że małego potworka trzeba nakarmić, sam decyduje o czasie, z reguły są to cztery rodzaje okazji lub ich kombinacja.


Nuda / Koncentracja - Dwa zupełne przeciwieństwa!
Stres / Relaks - dwa zupełne przeciwieństwa!

Jaki magiczny narkotyk może nagle odwrócić efekt, który wywołał kilka minut wcześniej? Prawda jest taka, że porno nie uwalnia od nudy i stresu, ani nie sprzyja koncentracji i relaksowi. Jeśli się nad tym zastanowić, to jakie inne okazje są w naszym życiu, poza snem? Jeśli masz pomysł, by przejść na inne rodzaje "realistycznego" lub "miękkiego" porno, pamiętaj, że treść tej książki odnosi się do wszystkich porno, drukowanych, z kamerkami, płatnych, czatów, programów na żywo itp. Ciało ludzkie jest najbardziej wyrafinowanym obiektem na naszej planecie, ale żaden gatunek, nawet najniższa ameba czy robak, nie przetrwa bez znajomości różnicy między jedzeniem a trucizną.

Dzięki naturalnej selekcji nasze umysły i ciała rozwinęły techniki nagradzania działań, które pomnażają i podtrzymują ludzkość. Nie są one przygotowane na nadnaturalne bodźce, które są większe, jaśniejsze i ostrzejsze niż wszystko, co można znaleźć w naturze. Nawet najbardziej stonowany dwuwymiarowy obraz powoduje, że stajemy się pobudzeni. Ale patrząc wielokrotnie na ten sam obraz, nie będziesz czuł podniecenia. W prawdziwym życiu, kontrola i równowaga zapewniają, że zrobisz coś innego, ale porno w Internecie nie ma takiego ogranicznika, powodując, że spędzisz swoje życie w wirtualnym haremie!

To fałsz, że ludzie słabi fizycznie i psychicznie stają się użytkownikami, a szczęśliwcami są ci, dla których pierwszy przypadek był odpychający i zostali wyleczeni na całe życie. Ewentualnie, nie są oni psychicznie przygotowani do przejścia przez ciężki proces uczenia się, walki o uzależnienie, obaw przed "złapaniem" lub nie są wystarczająco techniczni, aby obsługiwać ustawienia prywatności w przeglądarce. Być może najtragiczniejsza część tego całego biznesu dotyczy nastolatków - biegłych w wyszukiwaniu materiałów i zacieraniu śladów -- którzy zaczynają w coraz większej liczbie.

Cieszenie się porno w internecie to iluzja. Przeskakując z gatunku na gatunek, jedynie utrzymujemy naszą nowatorską 'małpkę' w obrębie 'czerwonej linii' 'bezpiecznych' gatunków porno, aby uzyskać dopaminową dawkę. Jak uzależnieni od heroiny, tak naprawdę cieszą się tylko z rytuału, który ma na celu złagodzenie tych bólów. 


## Taniec wokół czerwonej linii

Nawet z jednym klipem, który się utrzymał, użytkownicy ciągle uczą się odfiltrowywać złe i brzydkie części klipów porno. Nawet jeśli jest to solo, to i tak filtrują na części ciała, które najbardziej do nich przemawiają. W rzeczywistości, niektórzy czerpią przyjemność z tego tańca wokół czerwonej linii, znajdując wymówki, by zadeklarować, że lubią "miękkie rzeczy" i są  nieuzależnieni od nadprzyrodzonych bodźców. Ale zapytaj użytkownika który uważa, że trzyma się określonego aktora lub gatunku: *"Jeśli nie możesz dostać swojej normalnej marki porno i możesz dostać tylko niebezpieczny gatunek, czy przestajesz się masturbować?"*

Nie ma mowy! Użytkownik będzie się masturbował do wszystkiego, zmieniając gatunki, różnicach w orientacji seksualnej, podobnych wykonawcach, niebezpiecznych ustawieniach, szokujących relacjach, cokolwiek, co zaspokoi małego potwora. Na początku smakują okropnie, ale mając wystarczająco dużo czasu nauczysz się nimi cieszyć. Użytkownicy będą szukać pustego spełnienia po prawdziwym seksie, po długim dniu pracy, gorączce, przeziębieniach, grypie, bólach gardła, a nawet przy przyjęciu do szpitala. 

Przyjemność nie ma z tym nic wspólnego. Jeśli seks jest pożądany, to nie ma sensu robić to przy laptopie. Niektórzy użytkownicy uważają za niepokojące uświadomienie sobie, że są narkomanami i wierzą, że to sprawi, że jeszcze trudniej będzie przestać. W rzeczywistości jest to dobra wiadomość z dwóch ważnych powodów.

1.	Powodem, dla którego większość z nas kontynuuje używanie jest to, że chociaż wiemy, że wady znacznie przewyższają zalety, wierzymy, że jest coś w porno, co faktycznie sprawia nam przyjemność lub że działa ono jak pewnego rodzaju rekwizyt. Mamy złudzenie, że po zaprzestaniu używania porno pojawi się pustka, pewne sytuacje w naszym życiu nigdy nie będą takie same. W rzeczywistości porno nie tylko nic nie daje, ale wręcz odejmuje.

2.	Chociaż porno w Internecie jest najpotężniejszym wyzwalaczem dla nowości i seksu opartego na powodzi dopaminy, ze względu na szybkość z jaką stajesz się uzależniony, nigdy nie jesteś źle uzależniony. Rzeczywiste bóle odstawienia są tak łagodne, że większość użytkowników żyła i umarła nie zdając sobie sprawy, że ich doświadczyła.

Dlaczego więc wielu użytkownikom tak trudno jest przestać, przechodząc przez miesiące tortur i spędzając resztę życia na tęsknocie za nim w dziwnych momentach? Odpowiedzią jest drugi powód - pranie mózgu. Uzależnienie od neuroprzekaźnika jest łatwe do opanowania, większość użytkowników idzie na dni bez porno online na wyjazdach służbowych lub podróży, nie dotkniętych przez pangi odstawienia. Ich mały potwór jest bezpieczny, wiedząc, że otworzysz laptopa zaraz po powrocie do pokoju hotelowego. Możesz przetrwać swojego irytującego klienta i megalomańskiego menedżera, wiedząc, że lek na spadek nastroju jest na wyciągnięcie ręki.


## Analogia do palaczy

Dobrą analogią jest palacz papierosów. Gdyby przez dziesięć godzin w ciągu dnia nie wypalił papierosa, rwałby sobie włosy z głowy, ale wielu palaczy kupi nowy samochód i nigdy w nim nie zapali. Wielu z nich odwiedza teatry, supermarkety, kościoły, i brak możliwości palenia nie stanowi dla nich problemu. Nawet w pociągach i samolotach nie było żadnych zamieszek. Palacze są prawie zadowoleni z tego, że ktoś lub coś nie pozwala im palić.

Użytkownicy będą automatycznie powstrzymywać się od korzystania z pornografii internetowej w domu rodziców, podczas spotkań rodzinnych i innych wydarzeń z niewielkim dyskomfortem. W rzeczywistości, większość użytkowników ma wydłużone okresy, podczas których powstrzymują się bez wysiłku. Ten neurologiczny mały potwór jest łatwy do pokonania nawet wtedy, gdy nadal jest się uzależnionym. Istnieją miliony użytkowników, którzy przez całe życie pozostają zwykłymi użytkownikami i są tak samo uzależnieni jak ciężcy użytkownicy. Są nawet ciężcy użytkownicy którzy porzucili nałóg, ale od czasu do czasu zerkają na niego, smarując zjeżdżalnię, by zjechać z niej przy następnym spadku nastroju.

Jak wspomniano wcześniej, faktyczne uzależnienie od pornografii nie jest głównym problemem, po prostu działa w naszym umyśle jako zasłona przed prawdziwym problemem - praniem mózgu. Nie myślcie jednak, że złe skutki pornografii internetowej są wyolbrzymiane, jeśli w ogóle, to są one niestety bagatelizowane. Od czasu do czasu krążą pogłoski, że ścieżki neuronowe które powstały, są tam na całe życie, ale są one nieprawdziwe. Nasze mózgi i ciała są cudownymi maszynami, które odzyskują sprawność w ciągu kilku tygodni.

Nigdy nie jest za późno, by przestać! Szybkie przejrzenie społeczności internetowych pokaże ci ludzi w każdym wieku, którzy restartują swoje (i swoich partnerów) życie. Jak ze wszystkim, co robią ludzie, niektórzy z nich przechodzą na wyższy poziom, praktykując zatrzymywanie nasienia, Karezza i poprzez zróżnicowanie zmysłowych i propagacyjnych stron seksu sprawiają, że ich partnerzy są szczęśliwsi niż kiedykolwiek wcześniej.

Pocieszające dla wieloletnich i intensywnych użytkowników może być to, że równie łatwo jest im przestać jak zwykłym użytkownikom, a w pewien szczególny sposób jest to łatwiejsze. Im bardziej cię to ciągnie w dół, tym większa ulga. Kiedy przestałem, przeszedłem od razu do *zera* i nie miałem ani jednego złego uczucia. W rzeczywistości proces był przyjemny, nawet podczas okresu odstawienia.

It may be of consolation to lifelong and heavy users that it’s just as easy for them to stop as casual users, and in a peculiar way it’s easier. The further it drags you down, the greater the relief. When I stopped I went straight to *zero* and didn’t have one bad pang. In fact, the process was actually enjoyable even during the withdrawal period.

Ale najpierw musimy usunąć pranie mózgu. 
